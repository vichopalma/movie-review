class Movie < ApplicationRecord
    searchkick
    belongs_to :user
    has_many_attached :images
    has_many :reviews

    validates :title, presence: true
    validates :description, presence: true
    validate :image_type
    

    def thumbnail input
        return self.images[input].variant(resize: '400x600!').processed
    end

    private
    def image_type
        if images.attached? == false
            errors.add(:images, "Faltan las imagenes!")
        end
        images.each do |image|
            if !image.content_type.in?(%('image/jpeg'))
                errors.add(:images, 'needs to be a JPEG')
            end
        end
    end
end
